open Ocamlbuild_plugin

(* The code below is adapted from the OCamlbuild sources,
     src/main.ml
     https://github.com/ocaml/ocamlbuild/blob/6a7311b/src/main.ml#L121-L184
   with just a small part of it commented out
*)
let entry () =
  let target_dirs = List.union [] (List.map Pathname.dirname !Options.targets) in
  let open Ocamlbuild_pack in
  let entry = Slurp.filter
    begin fun path name () ->
      let dir =
        if path = Filename.current_dir_name then
          None
        else
          Some path
      in
      let path_name = path/name in

      if name = "_tags" then begin
        let tags_path =
          (* PR#6482: remember that this code is run lazily by the Slurp command,
             and may run only after the working directory has been changed.

             On the other hand, always using the absolute path makes
             error messages longer and more frigthening in case of
             syntax error in the _tags file. So we use the absolute
             path only when necessary -- the working directory has
             changed. *)
          if Sys.getcwd () = Pathname.pwd then path_name
          else Pathname.pwd / path_name in
        ignore (Configuration.parse_file ?dir tags_path);
      end;

      (List.mem name ["_oasis"]
       (* the commented test below would disable files starting with '_' *)
       || (String.length name > 0 (* && name.[0] <> '_' *)))
      && (name <> !Options.build_dir && not (List.mem name !Options.exclude_dirs))
      && begin
        not (path_name <> Filename.current_dir_name && Pathname.is_directory path_name)
        || begin
          let tags = tags_of_pathname path_name in
          (Tags.mem "include" tags
          || List.mem path_name !Options.include_dirs
          || Tags.mem "traverse" tags
          || List.exists (Pathname.is_prefix path_name) !Options.include_dirs
          || List.exists (Pathname.is_prefix path_name) target_dirs)
          && ((* beware: !Options.build_dir is an absolute directory *)
              Pathname.normalize !Options.build_dir
              <> Pathname.normalize (Pathname.pwd/path_name))
        end
      end
    end
    (Slurp.slurp Filename.current_dir_name)
  in
  let hygiene_entry =
    Slurp.map begin fun path name () ->
      let tags = tags_of_pathname (path/name) in
      not (Tags.mem "not_hygienic" tags) && not (Tags.mem "precious" tags)
    end entry in
  Slurp.force hygiene_entry;
  hygiene_entry

let () = dispatch begin function
| Before_rules ->
  let open Ocamlbuild_pack in
  Options.entry := Some (entry ())
| _ -> ()
end
